CURRENT_DIR=$(pwd)

run:
	go run cmd/main.go

swag-init:
	swag init -g api/router.go -o api/docs

proto-gen:
	sh ./scripts/gen_proto.sh

