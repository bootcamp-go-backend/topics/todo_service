package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"todo_service/genproto/protos"
	"todo_service/grpc/client"
	"todo_service/service"
	"todo_service/storage"
)

func SetUpServer(strg storage.IStorage, services client.IServiceManager) *grpc.Server {
	grpcServer := grpc.NewServer() // mistake was here

	todo_service.RegisterTodoServiceServer(grpcServer, service.NewTodoService(strg, services))
	todo_service.RegisterUserServiceServer(grpcServer, service.NewUserService(strg, services))

	reflection.Register(grpcServer)

	return grpcServer
}
