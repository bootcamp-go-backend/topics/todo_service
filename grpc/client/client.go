package client

import (
	"google.golang.org/grpc"
	"todo_service/config"
	"todo_service/genproto/protos"
)

type IServiceManager interface {
	TodoService() todo_service.TodoServiceClient
	UserService() todo_service.UserServiceClient
}

type grpcClients struct {
	todoService todo_service.TodoServiceClient
	userService todo_service.UserServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManager, error) {
	connTodoService, err := grpc.Dial(
		cfg.ServiceGprcHost+cfg.ServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		todoService: todo_service.NewTodoServiceClient(connTodoService),
		userService: todo_service.NewUserServiceClient(connTodoService),
	}, nil
}

func (g *grpcClients) TodoService() todo_service.TodoServiceClient {
	return g.todoService
}

func (g *grpcClients) UserService() todo_service.UserServiceClient {
	return g.userService
}
