package storage

import (
	"context"
	pb "todo_service/genproto/protos"
)

type IStorage interface {
	Close()
	Todo() ITodoStorage
}

type ITodoStorage interface {
	Create(context.Context, *pb.CreateTodoRequest) (*pb.Todo, error)
}
