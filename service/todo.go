package service

import (
	"context"
	"todo_service/genproto/protos"
	"todo_service/grpc/client"
	"todo_service/storage"
)

type todoService struct {
	strg     storage.IStorage
	services client.IServiceManager
	todo_service.UnimplementedTodoServiceServer
}

func NewTodoService(strg storage.IStorage, services client.IServiceManager) *todoService {
	return &todoService{
		strg:     strg,
		services: services,
	}
}

func (t *todoService) Create(ctx context.Context, request *todo_service.CreateTodoRequest) (*todo_service.Todo, error) {

	// logic
	return t.strg.Todo().Create(ctx, request)
}

func (t *todoService) Get(ctx context.Context, request *todo_service.PrimaryKey) (*todo_service.Todo, error) {
	return t.Get(ctx, request)
}
