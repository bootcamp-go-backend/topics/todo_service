package service

import (
	"context"
	"fmt"
	"todo_service/genproto/protos"
	"todo_service/grpc/client"
	"todo_service/storage"
)

type userService struct {
	strg     storage.IStorage
	services client.IServiceManager
	todo_service.UnimplementedUserServiceServer
}

func NewUserService(strg storage.IStorage, services client.IServiceManager) *userService {
	return &userService{
		strg:     strg,
		services: services,
	}
}

func (t *userService) Create(ctx context.Context, request *todo_service.CreateUserRequest) (*todo_service.User, error) {
	fmt.Println("req", request.Age, request.Name)

	return nil, nil
}

func (t *userService) Get(ctx context.Context, request *todo_service.PrimaryKey) (*todo_service.User, error) {
	return t.Get(ctx, request)
}
