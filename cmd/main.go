package main

import (
	"context"
	"github.com/gin-gonic/gin"
	"net"
	"todo_service/config"
	"todo_service/grpc"
	"todo_service/grpc/client"
	"todo_service/pkg/logger"
	"todo_service/storage/postgres"
)

func main() {
	cfg := config.Load()

	loggerLevel := logger.LevelDebug

	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.DebugMode)
	case config.TestMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.TestMode)
	default:
		loggerLevel = logger.LevelInfo
		gin.SetMode(gin.ReleaseMode)
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	pgStore, err := postgres.New(context.Background(), cfg)
	if err != nil {
		log.Error("error while connection to postgres", logger.Error(err))
		return
	}
	defer pgStore.Close()

	services, err := client.NewGrpcClients(cfg)
	if err != nil {
		log.Error("error while dialing grpc clients", logger.Error(err))
		return
	}

	grpcServer := grpc.SetUpServer(pgStore, services)

	lis, err := net.Listen("tcp", cfg.ServiceGprcHost+cfg.ServiceGrpcPort)
	if err != nil {
		log.Error("error while listening grpc host port", logger.Error(err))
		return
	}

	log.Info("Service is running...", logger.Any("grpc port", cfg.ServiceGrpcPort))
	if err = grpcServer.Serve(lis); err != nil {
		log.Error("error while listening grpc", logger.Error(err))
	}
}
